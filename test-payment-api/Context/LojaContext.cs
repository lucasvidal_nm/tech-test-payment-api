using Microsoft.EntityFrameworkCore;
using test_payment_api.Models;

namespace test_payment_api.Context;

public class LojaContext : DbContext
{
    public LojaContext(DbContextOptions<LojaContext> options) : base(options)
    {
        
    }

    public DbSet<Venda> Venda { get; set; }
    public DbSet<Vendedor> Vendedor { get; set; }
    public DbSet<Item> Item { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        

        modelBuilder.Entity<Venda>()
            .Property(x => x.StatusVenda)
            .HasConversion<string>();

        base.OnModelCreating(modelBuilder);
    }
}