﻿using AutoMapper;
using test_payment_api.DTO;
using test_payment_api.Models;

namespace test_payment_api.Profiles;

public class LojaProfile : Profile
{
    public LojaProfile()
    {
        CreateMap<AdicionaVendaDTO, Venda>();
        CreateMap<AtualizaVendaDTO, Venda>();

    }
}
