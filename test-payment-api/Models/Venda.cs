using System.ComponentModel.DataAnnotations;
namespace test_payment_api.Models;

public class Venda
{
    [Key]
    [Required]
    public int Id { get; set; }
    [Required]
    public int VendedorId { get; set; }
    [Required]
    public DateTime DataVenda { get; set; }
    [Required]
    public StatusVenda StatusVenda { get; set; }
}