using System.ComponentModel.DataAnnotations;

namespace test_payment_api.Models;

public class Vendedor
{
    [Key]
    [Required]
    public int Id { get; set; }
    [Required]
    public string Cpf { get; set; }
    [Required]
    public string Nome { get; set; }
    [Required]
    public string Email { get; set; }
    [Required]
    public string Telefone { get; set; }
}