﻿using System.ComponentModel.DataAnnotations;

namespace test_payment_api.Models
{
    public enum StatusVenda
    {
        
        [Display(Name = "AguardandoPagamento")]
        AguardandoPagamento,
        [Display(Name = "PagamentoAprovado")]
        PagamentoAprovado,
        [Display(Name = "EnviadoParaTransportadora")]
        EnviadoParaTransportadora,
        [Display(Name = "Entregue")]
        Entregue,
        [Display(Name = "Cancelada")]
        Cancelada
    }
}
