﻿using System.ComponentModel.DataAnnotations;

namespace test_payment_api.Models;

public class Item
{
    [Key]
    [Required]
    public int Id { get; set; }
    [Required]
    public string Nome { get; set; }
}
