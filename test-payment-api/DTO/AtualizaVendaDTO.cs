﻿using System.ComponentModel.DataAnnotations;
using test_payment_api.Models;

namespace test_payment_api.DTO
{
    public class AtualizaVendaDTO
    {
        [Required]
        public int VendaId { get; set; }
        [Required]
        [EnumDataType(typeof(StatusVenda))]
        public string StatusVenda { get; set; }
    }
}
