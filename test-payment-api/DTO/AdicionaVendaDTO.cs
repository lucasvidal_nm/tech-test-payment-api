using System.ComponentModel.DataAnnotations;

namespace test_payment_api.DTO;

public class AdicionaVendaDTO
{
    [Key]
    [Required]
    public int VendedorId { get; set; }
    [Required]
    [DisplayFormat(DataFormatString = "{0:dd/MM/yyy}")]
    public DateTime DataVenda { get; set; }
}