using System.ComponentModel.DataAnnotations;
using test_payment_api.Models.Enum;

namespace test_payment_api.DTO;

public class AdicionaVendaDTO
{
    [Key]
    [Required]
    public int VendedorId { get; set; }
    [Required]
    [DisplayFormat(DataFormatString = "{0:dd/MM/yyy}")]
    public DateTime DataVenda { get; set; }
    [Required]
    [EnumDataType(typeof(StatusVenda))]
    public object StatusVenda { get; set; }
}