using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using test_payment_api.Context;
using test_payment_api.DTO;
using test_payment_api.Models;

namespace test_payment_api.Controllers;

[ApiController]
[Route("[controller]")]
public class VendaController : ControllerBase
{
    private readonly LojaContext _context;
    private readonly IMapper _mapper;

    public VendaController(LojaContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }
    [HttpPost]
    public IActionResult AdicionarVenda([FromBody]AdicionaVendaDTO dto)
    {
        Venda venda = _mapper.Map<Venda>(dto);
        venda.StatusVenda = StatusVenda.AguardandoPagamento;
        _context.Venda.Add(venda);
        _context.SaveChanges();
        return Ok(venda);
    }

    [HttpGet]
    public IActionResult BuscarVenda(int vendaId)
    {

        var venda = _context.Venda.FirstOrDefault(x => x.Id == vendaId);
        _context.SaveChanges();
        return Ok(venda);
    }
    [HttpPut]
    public IActionResult AtualizaVenda(AtualizaVendaDTO dto)
    {

        var venda = _context.Venda.FirstOrDefault(x => x.Id == dto.VendaId);
        if(venda == null)
        {
            return NotFound();
        }
        var statusAnterior = venda.StatusVenda;

        foreach(var value in Enum.GetValues(typeof(StatusVenda)))
        {
            if(value.ToString() == dto.StatusVenda)
            {
                StatusVenda statusNovo = (StatusVenda)value;

                if (
                (statusAnterior == StatusVenda.AguardandoPagamento &&
                statusNovo != StatusVenda.PagamentoAprovado &&
                statusNovo != StatusVenda.Cancelada) ||

                (statusAnterior == StatusVenda.PagamentoAprovado &&
                statusNovo != StatusVenda.EnviadoParaTransportadora &&
                statusNovo != StatusVenda.Cancelada) ||

                (statusAnterior == StatusVenda.EnviadoParaTransportadora &&
                statusNovo != StatusVenda.Entregue
                )
        )
                {
                    throw new InvalidOperationException(message: "N�o � poss�vel alterar o Status da venda");
                }
                venda.StatusVenda = statusNovo;
                _context.SaveChanges();
                return Ok(venda);
            }
        }

        return Ok();
    }

}